from tkinter import*

cal = Tk()
operator=""
textInput = StringVar()

def __init__ (self):
    self.title("My calculator")

def btnClick (num):
    global operator
    operator = operator + str(num)
    textInput.set(operator)

def btnClear():
    global operator
    operator=""
    textInput.set("")

def btnEqual():
    global operator
    sum = str(eval(operator))
    textInput.set(sum)
    operator=""

txtDisplay = Entry(cal, font=('arial', 20, 'bold'), textvariable=textInput, bd=15, insertwidth=3, bg="powder blue", justify='right').grid(columnspan=4)

btn7 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="7", bg="powder blue", command=lambda:btnClick(7)).grid(row=1, column=0)        

btn8 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="8", bg="powder blue", command=lambda:btnClick(8)).grid(row=1, column=1)        

btn9 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="9", bg="powder blue", command=lambda:btnClick(9)).grid(row=1, column=2)

btnAdd = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="+", bg="powder blue", command=lambda:btnClick("+")).grid(row=1, column=3)   

#######################

btn6 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="6", bg="powder blue", command=lambda:btnClick(6)).grid(row=2, column=0)        

btn5 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="5", bg="powder blue", command=lambda:btnClick(5)).grid(row=2, column=1)        

btn4 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="4", bg="powder blue", command=lambda:btnClick(4)).grid(row=2, column=2)

btnSub = Button(cal, padx=32, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="-", bg="powder blue", command=lambda:btnClick("-")).grid(row=2, column=3)

########################

btn3 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="3", bg="powder blue", command=lambda:btnClick(3)).grid(row=3, column=0)        

btn2 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="2", bg="powder blue", command=lambda:btnClick(2)).grid(row=3, column=1)        

btn1 = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="1", bg="powder blue", command=lambda:btnClick(1)).grid(row=3, column=2)

btnMult = Button(cal, padx=28, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="x", bg="powder blue", command=lambda:btnClick("*")).grid(row=3, column=3)                       

#########################

btn0 = Button(cal, padx=30, pady=40, bd=5, fg="black", font=('arial', 20, 'bold'), text="0", bg="powder blue", command=lambda:btnClick(0)).grid(row=4, rowspan=2, column=0)        

btnClear = Button(cal, padx=30, pady=40, bd=5, fg="black", font=('arial', 20, 'bold'), text="C", bg="powder blue", command=btnClear).grid(row=4, rowspan=2, column=1)        

btnPower = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="^", bg="powder blue", command=lambda:btnClick("**")).grid(row=4, column=2)   

btnDiv = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="/", bg="powder blue", command=lambda:btnClick("/")).grid(row=4, column=3)   

#########################

btnModulo = Button(cal, padx=27, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="%", bg="powder blue", command=lambda:btnClick("%")).grid(row=5, column=3)   

btnEqual = Button(cal, padx=30, pady=10, bd=5, fg="black", font=('arial', 20, 'bold'), text="=", bg="powder blue", command=btnEqual).grid(row=5, column=2)   

cal.mainloop()
