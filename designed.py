from tkinter import *
from tkinter import ttk

class MyCalc (Frame):
    calcVal = 0.0
    div_trigger = False
    mult_trigger = False
    add_trigger = False
    sub_trigger = False
    modulo_trigger = False

    def clear(self, value):
        self.number_entry.delete(0, "end")

    def buttonPress(self, value):
 
        # Get current value in the entry
        entry_val = self.number_entry.get()
 
        # Put new value to the right of it
        # If it was 1 then 2 is pressed it is now 12
        # Otherwise new number goes on the left
        entry_val += value
 
        # Clears entry box
        self.number_entry.delete(0, END)
 
        # Insert new value going from left to right
        self.number_entry.insert(0, entry_val)
	
    def isFloat(self, str_val):
        try:
            float(str_val)
            return True
        except ValueError:
            return False

    def mathBtnPress (self, value):

        # Only do anything if entry currently contains a number
        if self.isFloat(str(self.number_entry.get())):

            # cancels out previous math button click
            self.add_trigger = False
            self.sub_trigger = False
            self.mult_trigger = False
            self.div_trigger = False
            self.modulo_trigger = False
            self.power_trigger = False

            # Get value out of entry box for calculation
            self.calcVal = float(self.entry_value.get())

            # Set the math button click so when equals is clicked
            # that function knows what calculation to use
            if value == "/":
                print("/ Pressed")
                self.div_trigger = True
            elif value == "*":
                print("* Pressed")
                self.mult_trigger = True
            elif value == "+":
                print("+ Pressed")
                self.add_trigger = True
            elif value == "%":
                print("% Pressed")
                self.modulo_trigger = True
            elif value == "**":
                print("** Pressed")
                self.modulo_trigger = True
            else:
                print("- Pressed")
                self.sub_trigger = True

            # Clears entry box
            self.number_entry.delete(0, "end")

    def equalBtnPress (self):

        # Make sure a math button was clicked
        if self.add_trigger or self.sub_trigger or self.mult_trigger or self.div_trigger or self.modulo_trigger:

            if self.add_trigger:
                solution = self.calcVal + float(self.entry_value.get())
            elif self.sub_trigger:
                solution = self.calcVal - float(self.entry_value.get())
            elif self.mult_trigger:
                solution = self.calcVal * float(self.entry_value.get())
            elif self.div_trigger:
                solution = self.calcVal / float(self.entry_value.get())
            elif self.modulo_trigger:
                solution = self.calcVal % float(self.entry_value.get())

            print(self.calcVal, " ", float(self.entry_value.get()),
                                            " ", solution)

            # Clear entry box
            self.number_entry.delete(0, "end")

            self.number_entry.insert(0, solution)

    def __init__(self, root):
        self.entry_value = StringVar(root, value="")

        root.title("My Calc")

        root.geometry("545x258")
        root.resizable(width=False, height=False)

        style = ttk.Style()
        style.configure("TButton", font="Serif 15", padding=8)
        style.configure("TEntry", font="Serif 30", padding=8)
        
        # create text entry box
        self.number_entry = ttk.Entry(root, justify='right', textvariable=self.entry_value, width=50)
        self.number_entry.grid(row=0, columnspan=4)

	# 1ST ROW
        self.btn7 = ttk.Button(root, text="7", command=lambda: self.buttonPress('7')).grid(row=1, column=0)
        
        self.btn8 = ttk.Button(root, text="8", command=lambda: self.buttonPress('8')).grid(row=1, column=1)

        self.btn9 = ttk.Button(root, text="9", command=lambda: self.buttonPress('9')).grid(row=1, column=2)

        self.button_div = ttk.Button(root, text="/", command=lambda: self.mathBtnPress('/')).grid(row=1, column=3)

	# 2nd Row
	
        self.btn4 = ttk.Button(root, text="4", command=lambda: self.buttonPress('4')).grid(row=2, column=0)
        
        self.btn5 = ttk.Button(root, text="5", command=lambda: self.buttonPress('5')).grid(row=2, column=1)

        self.btn6 = ttk.Button(root, text="6", command=lambda: self.buttonPress('6')).grid(row=2, column=2)

        self.button_div = ttk.Button(root, text="*", command=lambda: self.mathBtnPress('*')).grid(row=2, column=3)

        # 3rd Row
        
        self.button1 = ttk.Button(root, text="1", command=lambda: self.buttonPress('1')).grid(row=3, column=0)

        self.button2 = ttk.Button(root, text="2", command=lambda: self.buttonPress('2')).grid(row=3, column=1)

        self.button3 = ttk.Button(root, text="3", command=lambda: self.buttonPress('3')).grid(row=3, column=2)

        self.button_add = ttk.Button(root, text="+", command=lambda: self.mathBtnPress('+')).grid(row=3, column=3)

        # 4th Row
   
        self.button_clear = ttk.Button(root, text="AC", command=lambda: self.clear('AC')).grid(row=4, column=0)

        self.button0 = ttk.Button(root, text="0", command=lambda: self.buttonPress('0')).grid(row=4, column=1)

        self.button_equal = ttk.Button(root, text="=", command=lambda: self.equalBtnPress()).grid(row=4, column=2)

        self.button_sub = ttk.Button(root, text="-", command=lambda: self.mathBtnPress('-')).grid(row=4, column=3)

        # 5th Row

        self.button_modulo = ttk.Button(root, text="%", command=lambda: self.mathBtnPress('%')).grid(row=5, column=3)

root = Tk()

calc = MyCalc(root)

root.mainloop()


