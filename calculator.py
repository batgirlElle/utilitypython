from fractions import Fraction

def intro():
    print(''' Welcome to my Calculator! ''' )

def add_frac(a,b):
    print('Sum of fractions: {0}'.format(a+b))

def sub_frac(a,b):
    print('Difference of fractions: {0}'.format(a-b))

def mult_frac(x,y):
    print('Product: {0}'.format(x*y))

def div_frac(x,y):
    print('Quotient: {0}'.format(x/y))

def ask_frac():
    ans = input('''
Do you wish to use fractions (Y/N): ''')

    if ans.upper() == "Y":
        calc_frac()
    else:
        calculate()
        
def calc_frac():
    oper_frac = input('''Please type in the math operation you want to do:
+ for addition
- for subtraction
* for multiplication
/ for division
''')

    a = Fraction(input('Enter first fraction: '))
    b = Fraction(input('Enter second fraction: '))

    if oper_frac == "+":
        add_frac(a,b)
    elif oper_frac == "-":
        sub_frac(a,b)
    elif oper_frac == "*":
        mult_frac(a,b)
    elif oper_frac == "/":
        div_frac(a,b)
    
    ask()

def calculate():
    operation = input('''
Please type in what math operation you want to complete:
+ for addition
- for subtraction
* for multiplication
/ for division
% for modulo
** for power

''')

    num1 = int(input('Enter first num: '))
    num2 = int(input('Enter second num: '))

    if operation == "+":
        print('{} + {} = {}'.format(num1, num2, num1 + num2))
    elif operation == "-":
        print('{} - {} = {}'.format(num1, num2, num1 - num2))
    elif operation == "*":
        print('{} * {} = {}'.format(num1, num2, num1 * num2))
    elif operation == "/":
        print('{} / {} = {}'.format(num1, num2, num1 / num2))
    elif operation == "%":
        print('{} % {} = {}'.format(num1, num2, num1 % num2))
    elif operation == "**":
        print('{} ** {} = {}'.format(num1, num2, num1 ** num2))
    else:
        print('You have typed in an invalid operation. Run the program again.')

    ask()

def ask():
    # take input
    calc_ask = input(''' Do you want to calculate again? Please type Y for YES or N for NO: ''')

    if calc_ask.upper() == "Y":
        ask_frac()
    elif calc_ask.upper() == "N":
        print('Goodbye!')
    else:
        ask()

intro()
ask_frac()



